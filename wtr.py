#weather forecast

import pyowm

owm = pyowm.OWM('c915300b0018f66fd3f4221f567a4280')

place = input('In which city / country?: ')

observation = owm.weather_at_place(place)
w = observation.get_weather()

temp = w.get_temperature('celsius')['temp']

print('In the town ' + place + ' now ' + w.get_detailed_status())
print('The temperature is now around ' + str(temp))

if temp < 10:
    print("it's very cold now, dress as warm as possible!")
elif temp < 20:
    print("it's cold now, put on warm clothes.")
else:
    print('the temperature is normal, dress as you like.')


